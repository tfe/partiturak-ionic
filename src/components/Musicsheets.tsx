import {
  IonList,
  IonItem,
  IonIcon,
} from '@ionic/react';
import { Link } from 'react-router-dom';
import Locales from '../i18n/locales.js';
import Musiccard from './Musiccard.tsx';
import './Musicsheets.css';


function Musicsheets(props)
{
  return (
    <IonList className="list">
            {
                props.currentsheets.map((x,idx) =>
                {
                    return (
                        <Musiccard data={props.data} musicsheet={x} key={idx} />
                    );
                })
            }
    </IonList>
  );
};

export default Musicsheets;
