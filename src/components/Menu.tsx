import React, { useState } from 'react';
import {
  IonButton,
  IonAccordion,
  IonAccordionGroup,
  IonContent,
  IonListHeader,
  IonHeader,
  IonLabel,
  IonButtons,
  IonItem,
  IonMenu,
  IonMenuButton,
  IonMenuToggle,
  IonTitle,
  IonIcon,
  IonList,
  IonToolbar,
} from '@ionic/react';
import { Link } from 'react-router-dom';
import Header from '../components/Header';
import { home, folderOutline, personOutline, musicalNotesOutline } from 'ionicons/icons';

import type { RadioGroupCustomEvent } from '@ionic/react';
import Locales from '../i18n/locales.js';

import './Menu.css';

function Menu(props) {
    return (
    <>
      <IonMenu type="overlay" contentId="main-content">
        <Header />
        <IonContent className="ion-padding">

            <IonList className="">
                <IonListHeader>
                    <IonLabel className="menuItem">
                        <IonMenuToggle className="menuToggle">
                        <IonIcon className="menuIcon" aria-hidden="true" ios={home} md={home} />
                        <Link className="menuLink" to={"/home"}>{ Locales.home.menu }</Link>
                        </IonMenuToggle>
                    </IonLabel>
                </IonListHeader>
            </IonList>

            <IonAccordionGroup>
                <IonAccordion value="first">
                    <IonItem slot="header" color="light">
                        <IonIcon className="menuIcon" aria-hidden="true" ios={folderOutline} md={folderOutline} />
                        <IonLabel className="menuItem">{ Locales.categories }</IonLabel>
                    </IonItem>
                    <IonList className="ion-padding" slot="content">
                        {
                            props.data.tagsByName.map(function(id) {
                                let tag = props.data.tags[id];
                                if(tag.isType)
                                {
                                    return (
                                        <IonItem  key={'category-'+props.data.pageId+'-'+id}>
                                        <IonMenuToggle className="menuToggle">
                                            <Link className="menuLink" to={"/category/"+id}>{ tag.name } ({ props.data.musicsheetsByTags[id].length })</Link>
                                        </IonMenuToggle>
                                        </IonItem>
                                    );
                                }
                            })
                        }
                    </IonList>
                </IonAccordion>
                <IonAccordion value="second">
                    <IonItem slot="header" color="light">
                        <IonIcon className="menuIcon" aria-hidden="true" ios={personOutline} md={personOutline} />
                        <IonLabel className="menuItem">{ Locales.authors }</IonLabel>
                    </IonItem>
                    <IonList className="ion-padding" slot="content">
                        {
                            props.data.tagsByName.map(function(id) {
                                let tag = props.data.tags[id];
                                if(tag.isAuthor)
                                {
                                    return (
                                        <IonItem key={'author-'+props.data.pageId+'-'+id}>
                                        <IonMenuToggle className="menuToggle">
                                            <Link className="menuLink" to={"/author/"+id} >{ tag.name } ({ props.data.musicsheetsByTags[id]?.length ?? 0 })</Link>
                                        </IonMenuToggle>
                                        </IonItem>
                                    );
                                }
                            })
                        }
                    </IonList>
                </IonAccordion>
                <IonAccordion value="third">
                    <IonItem slot="header" color="light">
                        <IonIcon className="menuIcon" aria-hidden="true" ios={musicalNotesOutline} md={musicalNotesOutline} />
                        <IonLabel className="menuItem">{ Locales.instruments }</IonLabel>
                    </IonItem>
                    <IonList className="ion-padding" slot="content">
                        {
                            props.data.tagsByName.map(function(id) {
                                let tag = props.data.tags[id];
                                if(tag.isInstrument)
                                {
                                    return (
                                        <IonItem key={'instrument-'+props.data.pageId+'-'+id}>
                                        <IonMenuToggle className="menuToggle">
                                            <Link className="menuLink" to={"/instrument/"+id} >{ tag.name } ({ props.data.musicsheetsByTags[id].length })</Link>
                                        </IonMenuToggle>
                                        </IonItem>
                                    );
                                }
                            })
                        }
                    </IonList>
                </IonAccordion>
            </IonAccordionGroup>
        </IonContent>
      </IonMenu>
    </>
);
}

export default Menu;
