import React, { useState } from 'react';
import { 
  IonHeader,
  IonButton,
  IonToolbar,
  IonButtons,
  IonMenuButton,
  IonTitle,
} from '@ionic/react';
import type { RadioGroupCustomEvent } from '@ionic/react';
import Locales from '../i18n/locales.js';
import Menu from '../components/Menu';

function Header(props) {

    return (
            <>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton></IonMenuButton>
                    </IonButtons>
                    <IonTitle>{ Locales.appName }</IonTitle>
                </IonToolbar>
            </IonHeader>
            </>
        );
}

export default Header;
