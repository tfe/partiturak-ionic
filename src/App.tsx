import { Redirect, Route } from 'react-router-dom';
import { IonApp, IonRouterOutlet, setupIonicReact } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import Loading from './pages/Loading';
import Home from './pages/Home';
import TagList from './pages/TagList';
import View from './pages/View';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';

import Menu from './components/Menu';
import React, { useState, useEffect } from 'react';
setupIonicReact();
import Locales from './i18n/locales.js';


const App: React.FC = () => {

    const [ loaded , setLoaded ] =  useState(false);
    const [ data , setData ] =  useState({});

    const fetchData = () => {
        fetch("https://partiturak.eus/generic_data")
        .then(response => {
                    return response.json()
                    })
        .then(data => {

                console.log('set ',data);
                setData(data);
                setLoaded(true);
                })
    }

    useEffect(() => {
            fetchData()
    }, [])

    return (
      <IonApp>
        { ! loaded && (<Loading />) }
        { loaded && (
            <IonReactRouter>
                <Menu data={data} />
              <IonRouterOutlet>
                <Route exact path="/home">
                  <Home data={data} />
                </Route>
                <Route path="/category/:tagId">
                  <TagList data={data} />
                </Route>
                <Route path="/author/:tagId">
                  <TagList data={data} />
                </Route>
                <Route path="/instrument/:tagId">
                  <TagList data={data} />
                </Route>
                <Route path="/view/:id">
                  <View data={data} />
                </Route>
                <Route exact path="/">
                  <Redirect to="/home" />
                </Route>
              </IonRouterOutlet>
            </IonReactRouter>
        ) }
      </IonApp>
    );
};


export default App;
