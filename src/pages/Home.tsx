import React, { useState } from 'react';
import { 
  IonButton,
  IonContent,
  IonHeader,
  IonItem,
  IonLabel,
  IonMenu,
  IonMenuToggle,
  IonPage,
  IonRadio,
  IonRadioGroup,
  IonTitle,
  IonToolbar
} from '@ionic/react';
import type { RadioGroupCustomEvent } from '@ionic/react';
import Locales from '../i18n/locales.js';
import Header from '../components/Header';
import Musicsheets from '../components/Musicsheets';
import { useParams } from 'react-router-dom';

function Home(props) {

    let params = useParams();

    let musicsheets= [];
    let keys = Object.keys(props.data.musicsheets).reverse();
    let indexes = keys.slice(0, 20);

    indexes.forEach((key) => {
        props.data.musicsheets[key].id = key;
        musicsheets.push( props.data.musicsheets[key]);
    });

    return (
            <>
            <IonPage id="main-content">
                <Header />

                <IonContent className="ion-padding">
                    <h2>{ Locales.home.title }</h2>
                    <Musicsheets { ... props } currentsheets={musicsheets} />
                </IonContent>
            </IonPage>
            </>
           );
}
export default Home;
