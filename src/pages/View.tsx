import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
import { 
  IonButton,
  IonContent,
  IonHeader,
  IonItem,
  IonLabel,
  IonMenu,
  IonMenuToggle,
  IonPage,
  IonRadio,
  IonRadioGroup,
  IonTitle,
  IonTabs, IonTabBar, IonTabButton,
  IonSegment, IonSegmentButton,
  IonToolbar
} from '@ionic/react';
import type { RadioGroupCustomEvent } from '@ionic/react';
import Locales from '../i18n/locales.js';
import Menu from '../components/Menu';
import Header from '../components/Header';
import Musicsheets from '../components/Musicsheets';
import Tag from '../components/Tag';

import './View.css';

function View(props) {

    let params = useParams();
    let newProps = { ... props };
    newProps.pageId = props.type+'-'+params.id+'-content';

    let musicsheet = props.data.musicsheets[ params.id ];
    const [ pdf , setPdf ] =  useState(musicsheet.mainPdf);

    console.log('loaded',musicsheet);

    let updatePdf = function(x)
    {
        setPdf(x.target.value);
        console.log('received',x , x.target.value)
    }

    return (
            <>
            <IonPage id="main-content">
            <Header />
            <IonContent className="ion-padding">

            <h2 className="name">
                { musicsheet.name }
            </h2>
            <div className="tags">
            {
                musicsheet.tags.map((tagId) => {
                    let tag = props.data.tags[tagId];
                    tag.id = tagId;
                    return (<Tag tag={tag} />);
                })
            }

            </div>

            {
                (musicsheet.pdfs || musicsheet.mainPdf) &&
                <IonSegment value={pdf} scrollable={true} >
                <IonSegmentButton onClick={updatePdf} value={musicsheet.mainPdf}>{musicsheet.name}</IonSegmentButton>
                    {
                        musicsheet.pdfs.map((pdf) => {
                        return (<IonSegmentButton onClick={updatePdf} value={pdf.pdf}>{pdf.name}</IonSegmentButton>)
                        })
                    }
                </IonSegment>
            }

            <div className="pdf_preview">
            <img src={"https://partiturak.eus/"+pdf.replace('.pdf','_thumb.jpg')} />
            </div>

            </IonContent>

            </IonPage>
            </>
           );
}
export default View;
